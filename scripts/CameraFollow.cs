﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class CameraFollow : Singleton<CameraFollow>
{
    public Transform target;
    public float moveSmooth = 0.5f;
    public float rotateSmooth = 2f;
    public float minHeight = 5;
    public float maxAngle = 80;
    public float minAngle=40;
    public float horizontalRotationSpeed = 50;
    public float verticalRotationSpeed = 25;
    public float inputMovingSpeed = 10;
    [Tooltip("Дистанция до новой цели, при которой камера не возвращается в исходную точку")]
    public float easyFollowDistance=10;
    [Range(5,15)]public float minDistance;
    [Range(10.5f,30)]public float maxDistance;
    [HideInInspector]public Vector3 offset;
    [HideInInspector]public Vector3 globalOffset;
    Vector3 localOffset;
    public float basicDistance=10;
    float desiredDistance;
    bool manual = false;
    Dictionary<string, System.Action> inputsDict;
    Coroutine waiting, following,looking,rotating,moving;
    protected override void Awake()
    {
        base.Awake();
        ArrowButton.ArrowPress += ArrowInput;
        ArrowButton.ArrowUp += InputStop;
        InitDict();
        transform.position = globalOffset;
    }
    public void Init()
    {

        InitDistances();

        waiting = StartCoroutine(WaitingForTarget());

    }
    void InitDistances()
    {

        desiredDistance = basicDistance;
        Debug.Log($"Basic distance is {basicDistance}");
    }
    void InitDict()
    {
        inputsDict = new Dictionary<string, System.Action>
        {
            ["left"] = new System.Action(() => InputRotate(horizontalRotationSpeed,true)),
            ["right"] = new System.Action(() => InputRotate(-horizontalRotationSpeed,true)),
            ["up"]=new System.Action(()=> InputRotate(-verticalRotationSpeed, false)),
            ["down"]=new System.Action(()=> InputRotate(verticalRotationSpeed, false)),
            ["plus"]=new System.Action(() => InputMove(-inputMovingSpeed)),
            ["minus"]= new System.Action(() => InputMove(inputMovingSpeed))
        };
    }

    void ArrowInput(string key)
    {
        if (target == null)
            return;

        InputStop();
        inputsDict[key]?.Invoke();
    }

    void OnTargetFound()
    {
        StartCoroutine(GenerateLocalOffset());
        following =StartCoroutine(Following(target));
        looking = StartCoroutine(LookingOnTarget(target));
    }

    public void CalculateOffset()
    {
        offset = transform.position - target.position;
    }
    IEnumerator WaitingForTarget()
    {
        while(target==null)
        {
            transform.Rotate(Vector3.up, rotateSmooth * Time.deltaTime * 3, Space.World);
            yield return null;
        }
        OnTargetFound();
    }
    IEnumerator LookingOnTarget(Transform target)
    {
        while(target==this.target)
        {
            var targetRot = Quaternion.LookRotation(target.position - transform.position);
            Quaternion newLook = Quaternion.Slerp(transform.rotation, targetRot,rotateSmooth*Time.deltaTime);
            transform.rotation = newLook;
            
            yield return null;
        }
        looking = StartCoroutine(LookingOnTarget(this.target));
        yield break;
    }
    void InputStop()
    {
        manual = true;
        if(rotating!=null)
            StopCoroutine(rotating);
        if (moving != null)
            StopCoroutine(moving);
    }
    void InputRotate(float angle,bool horizontal)
    {
        if (horizontal)
            rotating = StartCoroutine(RotatingHorizontal(target, angle));
        else
            rotating=StartCoroutine(RotatingVertical(target,angle));
    }
    void InputMove(float number)
    {
        moving = StartCoroutine(Moving(number));
    }
    IEnumerator RotatingVertical(Transform target, float angle)
    {
        while (this.target == target)
        {
            Vector3 diff = transform.position - target.position;
            Vector3 axis = Quaternion.AngleAxis(90, Vector3.up) * diff;
            axis.y = target.transform.position.y;
            float diffAngle = Vector3.Angle(diff, new Vector3(diff.x, 0, diff.z));
            Debug.Log($"Diff angle is{diffAngle}");
            if(diffAngle>minAngle&&angle>0||diffAngle<maxAngle&&angle<0)
                transform.RotateAround(target.position, axis, angle * rotateSmooth * Time.deltaTime);
            UpdateLocalOffset();
            yield return null;
        }
    }
    IEnumerator RotatingHorizontal(Transform target,float angle)
    {
        while(this.target==target)
        {
            transform.RotateAround(target.position, Vector3.up, angle*rotateSmooth*Time.deltaTime);
            UpdateLocalOffset();
            yield return null;
        }
    }
    IEnumerator Moving(float number)
    {
        while (desiredDistance!=maxDistance||desiredDistance!=minDistance)
        {
            desiredDistance = Mathf.Clamp(desiredDistance+number * moveSmooth * Time.deltaTime,minDistance,maxDistance);
            Debug.Log($"Desired distance is {desiredDistance}");
            UpdateLocalOffset();
            yield return null;
        }
    }
    public void SetGlobalOffset()
    {
        globalOffset = transform.position;
    }
    IEnumerator Following(Transform target)
    {
        while (target == this.target)
        {
            FollowByDistance();
            yield return null;
        }
        if (Vector3.SqrMagnitude(target.position - this.target.position) > easyFollowDistance * easyFollowDistance)
        {
            while (Vector3.SqrMagnitude(transform.position - globalOffset) >300)
            {
                MoveTo(globalOffset);
                yield return null;
            }
        }
        manual = false;
        following = StartCoroutine(Following(this.target));
        yield break;
    }
    IEnumerator GenerateLocalOffset()
    {
        while (true)
        {
            if (!manual)
            {
                float angle = Vector3.Angle(new Vector3(offset.x, target.position.y, offset.z), target.forward);
                Quaternion rot = Quaternion.AngleAxis(-angle, Vector3.up);
                localOffset = rot * offset;
            }
            yield return null;
        }
    }
    void UpdateLocalOffset()
    {
        localOffset =transform.position -target.position ;
    }

    void FollowByDistance()
    {
        float distance = Vector3.SqrMagnitude(transform.position - target.position);
        if(desiredDistance*desiredDistance!=distance)
        {

            MoveTo(Vector3.LerpUnclamped(target.position,target.position+localOffset,desiredDistance*desiredDistance/distance));

        }
    }
    void MoveTo(Vector3 to)
    {
        Vector3 newPos = Vector3.Lerp(transform.position, to, moveSmooth * Time.deltaTime);
        newPos.y=Mathf.Clamp(newPos.y, minHeight, 200);
        transform.position = newPos; 
    }
}
