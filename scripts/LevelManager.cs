﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class LevelManager : Singleton<LevelManager>
{
    private void Start()
    {
        AnimalManager.Instance.Init();
        CameraFollow.Instance.Init();
        AdsManager.Instance.Init();
    }
}
