﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class Singleton<T> : MonoBehaviour where T:Singleton<T>
{
    public static T Instance { get; set; }
    
    // Start is called before the first frame update
    protected virtual void Awake()
    {
        if(Instance==null)
        {
            Instance = this as T;
        }
        else if(Instance!=this)
        {
            Destroy(gameObject);
        }
        
    }
    private void OnDisable()
    {
        Instance = null;
    }

}
