﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class MusicPlayer : Singleton<MusicPlayer>
{
    public AudioClip[] clips;
    public AudioSource source;
    int currentTrack;
    private void Start()
    {
        source = GetComponent<AudioSource>();
        currentTrack = Random.Range(0, clips.Length);
        source.clip = clips[currentTrack];
        source.Play();
    }
    public void SwitchTrack()
    {
        source.Stop();
    }
    void NextTrack()
    {
        currentTrack++;
        if (currentTrack >= clips.Length)
            currentTrack = 0;
        source.clip = clips[currentTrack];
        source.Play();
    }
    IEnumerator Waiting()
    {
        while(true)
        {
            if(!source.isPlaying)
            {
                NextTrack();
            }
            yield return null;
        }
    }
}
