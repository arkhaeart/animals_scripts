﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class AnimalManager : Singleton<AnimalManager>
{
    readonly Dictionary<int, string> animalKeyDict = new Dictionary<int, string>
    {
        [0] = "pig",
        [1]="cow",
        [2]="dog",
        [3]="chicken",
        [4]="duck",
        [5]="goat",
        [6]="horse",
        [7]="sheep",
        [8]="puppy",
        [9]="cat"
    };
    public static List<Animal> registry = new List<Animal>();
    Animal current;
    List<Animal> animals = new List<Animal>();
    Dictionary<string,List<Animal>> animalSpecies = new Dictionary<string, List<Animal>>();
    // Start is called before the first frame update
    public void Init()
    {
        foreach(var spec in animalKeyDict.Values)
        {
            animalSpecies.Add(spec, new List<Animal>());
        }
        foreach(var animal in registry)
        {
            animals.Add(animal);
            PlaceAnimalBySpecies(animal);
        }
        UIManager.Instance.OnVoice += PlayVoice;

    }
    void PlayVoice()
    {
        if(current!=null)
            current.Voice();
    }
    void PlaceAnimalBySpecies(Animal animal)
    {
        string[] split = animal.name.Split('_');
        if(animalSpecies.ContainsKey(split[0]))
        {
            animalSpecies[split[0]].Add(animal);
            Debug.Log($"Animal {animal.name} added to {split[0]} species");
        }
    }
    
    public Animal GetRandomFromSpecies(int i)
    {
        string key = animalKeyDict[i];
        current= animalSpecies[key][Random.Range(0, animalSpecies[key].Count)];
        return current;
    }
    public Animal GetByIndex(int i)
    {
        current =animals[i];
        return current;
    }
    public Animal GetRandom()
    {
        int i = Random.Range(0, animals.Count);
        return GetByIndex(i);
    }
    // Update is called once per frame
    void Update()
    {
        
    }
}
