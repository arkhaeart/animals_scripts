﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.Advertisements;

public class AdsManager : Singleton<AdsManager>
{
    public event System.Action<bool> OnAdsPlaying;
    public float adsTime = 15;
    private string[] adPlacements = new[] { "personalizedPlacement", "rewardedVideo", "banner", "video" };
    WaitForSeconds adWait;
    bool testMode = false;
#if UNITY_IOS
    private string gameId = "3494608";
#elif UNITY_ANDROID
    private string gameId = "3494609";
#endif
    public bool Ad { get => Advertisement.isShowing; }
    protected override void Awake()
    {

        base.Awake();
        adWait = new WaitForSeconds(adsTime);
    }
    public void Init()
    {

        Advertisement.Initialize(gameId, testMode);
        StartCoroutine(AdPlaying());
    }
    IEnumerator AdPlaying()
    {
        while (true)
        {
            Debug.Log("Waiting for new ad");
            yield return adWait;
            Show();
            Debug.Log("Waiting for ad finish");
            yield return new WaitUntil(()=> !Ad);
            yield return null;
            OnAdsPlaying?.Invoke(true);
            
        }
    }
    public void Show()
    {
        OnAdsPlaying?.Invoke(true);
        if (Advertisement.isSupported && Advertisement.IsReady() && Advertisement.isInitialized)
        {

            Advertisement.Show("video");
            Debug.Log("Ad shown");
            
        }
    }


}
