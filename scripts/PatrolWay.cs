﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class PatrolWay : MonoBehaviour
{
    [HideInInspector] public PatrolPoint[] patrolPoints;
    void FindPatrolPoints()
    {

        patrolPoints = GetComponentsInChildren<PatrolPoint>(true);
    }
    private void OnDrawGizmos()
    {
        if(patrolPoints==null)
        {
            return;
        }
        if(transform.childCount!=patrolPoints.Length)
        {
            FindPatrolPoints();
        }
        if (patrolPoints.Length > 1)
        {
            for (int i = 0; i < patrolPoints.Length; i++)
            {
                
                if (i + 1 >= patrolPoints.Length)
                {
                    if (patrolPoints[0].obstacle)
                        Gizmos.color = Color.red;
                    else
                        Gizmos.color = Color.blue;
                    Gizmos.DrawLine(patrolPoints[i].transform.position, patrolPoints[0].transform.position);

                }
                else
                {
                    if (patrolPoints[i+1].obstacle)
                        Gizmos.color = Color.red;
                    else
                        Gizmos.color = Color.blue;
                    Gizmos.DrawLine(patrolPoints[i].transform.position, patrolPoints[i + 1].transform.position);

                }
            }
        }
    }
}
