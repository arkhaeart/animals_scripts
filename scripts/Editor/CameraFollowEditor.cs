﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEditor;
[CustomEditor(typeof(CameraFollow))]
public class CameraFollowEditor : Editor
{
    CameraFollow Obj => target as CameraFollow;
    public override void OnInspectorGUI()
    {
        DrawDefaultInspector();
        serializedObject.Update();
        GUILayout.BeginHorizontal();
        if(GUILayout.Button("Set local offset"))
        {
            if(Obj.target==null)
            {
                Debug.LogError("Choose transform to set local offset!");
            }
            Obj.CalculateOffset();
        }
        if(GUILayout.Button("Set global offset"))
        {
            Obj.SetGlobalOffset();
        }
        GUILayout.EndHorizontal();
        serializedObject.ApplyModifiedProperties();
    }
}
