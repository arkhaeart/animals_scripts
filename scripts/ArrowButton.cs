﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;
using UnityEngine.EventSystems;
public class ArrowButton : MonoBehaviour,IPointerDownHandler,IPointerUpHandler
{
    public static event System.Action<string> ArrowPress;
    public static event System.Action ArrowUp;

    public string key;
    Image image;
    Button button;
     
    public void OnPointerDown(PointerEventData eventData)
    {
        Debug.Log("pressed");
        ArrowPress(key);
    }

    public void OnPointerUp(PointerEventData eventData)
    {
        Debug.Log("unpressed");
        ArrowUp();
    }

    // Start is called before the first frame update
    void Awake()
    {
        button = GetComponent<Button>();
        image = transform.GetChild(0).GetComponent<Image>();
    }

}
