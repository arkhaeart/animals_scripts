﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.AI;

public class Animal : MonoBehaviour {
    public PatrolWay way;
    public Material[] usedMaterials;
    new SkinnedMeshRenderer renderer;
    private NavMeshAgent agent;
    static readonly string[] animKeys =  { "Eat", "Dig", "Sit","Shake" };
    const string walkKey="Walk";
    const string jumpKey = "Jump";
    const string voiceKey="Voice";
    Animator animator;
    Vector3 currentTarget;
    WaitForSeconds longWait;
    new Collider collider;
    new AudioSource audio;
    new Rigidbody rigidbody;
    private void Awake()
    {
        agent = gameObject.GetComponent<NavMeshAgent>();
        animator = GetComponent<Animator>();
        renderer = GetComponentInChildren<SkinnedMeshRenderer>(true);
        audio = GetComponent<AudioSource>();
        rigidbody = GetComponent<Rigidbody>();
        collider = GetComponent<Collider>();
        longWait = new WaitForSeconds(3.5f);
        renderer.material = usedMaterials[Random.Range(0, usedMaterials.Length)];
        AnimalManager.registry.Add(this);
    }
    void Start() {

        if (way != null)
            StartCoroutine(Patrolling());
        else
            Debug.LogError($"{name} doesn't have patrol way! Please set it.");
    }


    //public static bool DoesTagExist(string aTag) {
    //    try {
    //        GameObject.FindGameObjectsWithTag(aTag);
    //        return true;
    //    }
    //    catch {
    //        return false;
    //    }
    //}
    void SetNextTarget(int i)
    {
        if (!agent.enabled)
            return;
        agent.ResetPath();
        currentTarget = way.patrolPoints[i].transform.position;
        
        
        if (way.patrolPoints[i].obstacle)
        {
            animator.SetTrigger(jumpKey);
            StartCoroutine(Jumping());
        }
        else
        {
            animator.SetTrigger(walkKey);
            agent.SetDestination(currentTarget);
        }
        
    }
    IEnumerator Jumping()
    {
        Debug.Log("Agent deactivated on " + name);
        agent.enabled = false;
        //yield return StartCoroutine(Rotating());
        Vector3[] arc = new Vector3[4];
        arc[0] = Vector3.Lerp(transform.position, currentTarget, 0.3f) + Vector3.up * 1.25f;
        arc[1] = Vector3.Lerp(transform.position, currentTarget, 0.6f) + Vector3.up * 1.5f;
        arc[2] = Vector3.Lerp(transform.position, currentTarget, 0.8f) + Vector3.up * 1.15f;
        arc[3] = currentTarget;

        for (int i = 0; i < arc.Length; i++)
        {
            while(transform.position!=arc[i])
            {

                Vector3 newPos = Vector3.MoveTowards(transform.position, arc[i], 5*Time.deltaTime);

                transform.position = newPos;
                yield return null;
                //rigidbody.MovePosition(newPos);
            }
        }

        Debug.Log("Agent reactivated on " + name);
        agent.enabled = true;
        animator.SetTrigger(walkKey);
    }
    //IEnumerator Rotating()
    //{
    //    for (int i = 0; i < 10; i++)
    //    {
    //        var targetRot = Quaternion.LookRotation(currentTarget - transform.position);
    //        Quaternion newLook = Quaternion.Slerp(transform.rotation, targetRot, 0.2f);
    //        transform.rotation = newLook;
    //        yield return null;
    //    }
    //}
    IEnumerator Patrolling()
    {
        int currentPoint = Random.Range(0,way.patrolPoints.Length);
        SetNextTarget(currentPoint);
        while(true)
        {
            if (agent.enabled)
            {
                if (Vector3.SqrMagnitude((transform.position - currentTarget)) <= 0.5f)
                {

                    string key = animKeys[Random.Range(0, animKeys.Length)];

                    animator.SetTrigger(key);
                    yield return longWait;
                    currentPoint++;

                    if (currentPoint >= way.patrolPoints.Length)
                        currentPoint = 0;
                    SetNextTarget(currentPoint);
                }
            }
            yield return null;
        }

    }
    public void Voice()
    {
        if(agent.enabled)
            agent.isStopped = true;
        animator.SetTrigger(voiceKey);
        audio.Play();
        Invoke("ContinuePath", 2f);
    }
    void ContinuePath()
    {
        animator.SetTrigger(walkKey);
        if(agent.enabled)
            agent.isStopped=false;
    }
    //private void OnTriggerEnter(Collider patrolPoint) {
    //    if (!DoesTagExist(patrolPoint.tag) || patrolPoint.tag == "Untagged") return;
        
    //    int tagIndex = int.Parse(patrolPoint.tag);
    //    if (tagIndex == patrolPoints.Length - 1) {
    //        agent.SetDestination(patrolPoints[0].position);
    //    } else {
    //        agent.SetDestination(patrolPoints[tagIndex + 1].position);
    //    }
    //}
}
