﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;

public class UIManager : Singleton<UIManager>
{
    public Button[] controls,animals;
    public event System.Action OnVoice;
    public RectTransform HUD;
    public RectTransform animalWindow;
    public RectTransform foldArrow;
    int desiredHUD=1;
    Coroutine HudMoving;
    private void Start()
    {
        //InitialHUDPlacement();
        SetButtonsInteractable(false,controls);
        //AdsManager.Instance.OnAdsPlaying += (n) => { SetButtonsInteractable(n, controls); SetButtonsInteractable(n, animals); };
    }
    void InitialHUDPlacement()
    {
        Vector3 desiredPos = HUD.position + Vector3.up * animalWindow.rect.height*-1;
        HUD.position = desiredPos;
        desiredHUD = -1;
    }
    public void ToggleHUD()
    {
        //Rect rect = new Rect(HUD.rect);
        //rect.yMin = 0;
        desiredHUD = desiredHUD == -1 ? 1 : -1;
        foldArrow.Rotate(Vector3.forward, 180, Space.Self);
        Vector3 desiredPos = HUD.position + Vector3.up * desiredHUD*animalWindow.rect.height;
        if (HudMoving != null)
        {
            StopCoroutine(HUDMoving(desiredPos));
            HudMoving = null;
        }
        HudMoving=StartCoroutine(HUDMoving(desiredPos));
    }
    public void VoiceOnCurrentAnimal()
    {
        OnVoice?.Invoke();
    }
    public void RandomAnimal()
    {
        CameraFollow.Instance.target = AnimalManager.Instance.GetRandom().transform;
    }
    public void AnimalChosen(int index)
    {
        SetButtonsInteractable(true,controls);
        CameraFollow.Instance.target = AnimalManager.Instance.GetRandomFromSpecies(index).transform;
    }
    void SetButtonsInteractable(bool active,Button[] buttons)
    {
        foreach(var button in buttons)
        {
            button.interactable = active;
            button.GetComponent<Image>().raycastTarget = active;
        }
    }
    IEnumerator HUDMoving(Vector3 target)
    {
        while(Vector3.SqrMagnitude(HUD.position-target)>5)
        {
            Vector3 newPos = Vector3.MoveTowards(HUD.position, target, HUD.rect.height * Time.deltaTime);
            HUD.position = newPos;
            yield return null;
        }
    }

}
